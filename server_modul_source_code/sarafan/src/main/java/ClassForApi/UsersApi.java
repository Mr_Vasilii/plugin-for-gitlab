package ClassForApi;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"openissues",
"closeissues",
"pastdueissues",
"assigneeissues",
"averagecostopenissues",
"averagecostcloseissues",
"averagecostpastdueissues",
"averagecostassigneeissues",
"densityopenissues",
"densitycloseissues",
"densitypastdueissues",
"densityassigneeissues"
})
public class UsersApi {

@JsonProperty("name")
private String name;
@JsonProperty("openissues")
private String openissues;
@JsonProperty("closeissues")
private String closeissues;
@JsonProperty("pastdueissues")
private String pastdueissues;
@JsonProperty("assigneeissues")
private String assigneeissues;
@JsonProperty("averagecostopenissues")
private String averagecostopenissues;
@JsonProperty("averagecostcloseissues")
private String averagecostcloseissues;
@JsonProperty("averagecostpastdueissues")
private String averagecostpastdueissues;
@JsonProperty("averagecostassigneeissues")
private String averagecostassigneeissues;
@JsonProperty("densityopenissues")
private String densityopenissues;
@JsonProperty("densitycloseissues")
private String densitycloseissues;
@JsonProperty("densitypastdueissues")
private String densitypastdueissues;
@JsonProperty("densityassigneeissues")
private String densityassigneeissues;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("openissues")
public String getOpenissues() {
return openissues;
}

@JsonProperty("openissues")
public void setOpenissues(String openissues) {
this.openissues = openissues;
}

@JsonProperty("closeissues")
public String getCloseissues() {
return closeissues;
}

@JsonProperty("closeissues")
public void setCloseissues(String closeissues) {
this.closeissues = closeissues;
}

@JsonProperty("pastdueissues")
public String getPastdueissues() {
return pastdueissues;
}

@JsonProperty("pastdueissues")
public void setPastdueissues(String pastdueissues) {
this.pastdueissues = pastdueissues;
}

@JsonProperty("assigneeissues")
public String getAssigneeissues() {
return assigneeissues;
}

@JsonProperty("assigneeissues")
public void setAssigneeissues(String assigneeissues) {
this.assigneeissues = assigneeissues;
}

@JsonProperty("averagecostopenissues")
public String getAveragecostopenissues() {
return averagecostopenissues;
}

@JsonProperty("averagecostopenissues")
public void setAveragecostopenissues(String averagecostopenissues) {
this.averagecostopenissues = averagecostopenissues;
}

@JsonProperty("averagecostcloseissues")
public String getAveragecostcloseissues() {
return averagecostcloseissues;
}

@JsonProperty("averagecostcloseissues")
public void setAveragecostcloseissues(String averagecostcloseissues) {
this.averagecostcloseissues = averagecostcloseissues;
}

@JsonProperty("averagecostpastdueissues")
public String getAveragecostpastdueissues() {
return averagecostpastdueissues;
}

@JsonProperty("averagecostpastdueissues")
public void setAveragecostpastdueissues(String averagecostpastdueissues) {
this.averagecostpastdueissues = averagecostpastdueissues;
}

@JsonProperty("averagecostassigneeissues")
public String getAveragecostassigneeissues() {
return averagecostassigneeissues;
}

@JsonProperty("averagecostassigneeissues")
public void setAveragecostassigneeissues(String averagecostassigneeissues) {
this.averagecostassigneeissues = averagecostassigneeissues;
}

@JsonProperty("densityopenissues")
public String getDensityopenissues() {
return densityopenissues;
}

@JsonProperty("densityopenissues")
public void setDensityopenissues(String densityopenissues) {
this.densityopenissues = densityopenissues;
}

@JsonProperty("densitycloseissues")
public String getDensitycloseissues() {
return densitycloseissues;
}

@JsonProperty("densitycloseissues")
public void setDensitycloseissues(String densitycloseissues) {
this.densitycloseissues = densitycloseissues;
}

@JsonProperty("densitypastdueissues")
public String getDensitypastdueissues() {
return densitypastdueissues;
}

@JsonProperty("densitypastdueissues")
public void setDensitypastdueissues(String densitypastdueissues) {
this.densitypastdueissues = densitypastdueissues;
}

@JsonProperty("densityassigneeissues")
public String getDensityassigneeissues() {
return densityassigneeissues;
}

@JsonProperty("densityassigneeissues")
public void setDensityassigneeissues(String densityassigneeissues) {
this.densityassigneeissues = densityassigneeissues;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}