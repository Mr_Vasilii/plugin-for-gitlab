package ClassForApi;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"title",
"project",
"hours",
"state",
"duedate",
"userto",
"usersto"
})
public class IssuesApi {

@JsonProperty("title")
private String title;
@JsonProperty("project")
private String project;
@JsonProperty("hours")
private String hours;
@JsonProperty("state")
private String state;
@JsonProperty("duedate")
private String duedate;
@JsonProperty("userto")
private String userto;
@JsonProperty("usersto")
private String usersto;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@JsonProperty("project")
public String getProject() {
return project;
}

@JsonProperty("project")
public void setProject(String project) {
this.project = project;
}

@JsonProperty("hours")
public String getHours() {
return hours;
}

@JsonProperty("hours")
public void setHours(String hours) {
this.hours = hours;
}

@JsonProperty("state")
public String getState() {
return state;
}

@JsonProperty("state")
public void setState(String state) {
this.state = state;
}

@JsonProperty("duedate")
public String getDuedate() {
return duedate;
}

@JsonProperty("duedate")
public void setDuedate(String duedate) {
this.duedate = duedate;
}

@JsonProperty("userto")
public String getUserto() {
return userto;
}

@JsonProperty("userto")
public void setUserto(String userto) {
this.userto = userto;
}

@JsonProperty("usersto")
public String getUsersto() {
return usersto;
}

@JsonProperty("usersto")
public void setUsersto(String usersto) {
this.usersto = usersto;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
