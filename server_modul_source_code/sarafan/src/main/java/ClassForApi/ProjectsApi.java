package ClassForApi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"name",
"openissues",
"closeissues",
"pastdueissues",
"allissues",
"averagecostopenissues",
"averagecostcloseissues",
"averagecostpastdueissues",
"averagecostallissues",
"densityopenissues",
"densitycloseissues",
"densitypastdueissues",
"densityallissues",
"hours",
"users",
"usersopenissues",
"userscloseissues",
"userspastdueissues",
"usersassigneeissues",
"usersaveragecostopenissues",
"usersaveragecostcloseissues",
"usersaveragecostpastdueissues",
"usersaveragecostassigneeissues",
"usersdensityopenissues",
"usersdensitycloseissues",
"usersdensitypastdueissues",
"usersdensityassigneeissues"
})
public class ProjectsApi {

@JsonProperty("id")
private String id;
@JsonProperty("name")
private String name;
@JsonProperty("openissues")
private String openissues;
@JsonProperty("closeissues")
private String closeissues;
@JsonProperty("pastdueissues")
private String pastdueissues;
@JsonProperty("allissues")
private String allissues;
@JsonProperty("averagecostopenissues")
private String averagecostopenissues;
@JsonProperty("averagecostcloseissues")
private String averagecostcloseissues;
@JsonProperty("averagecostpastdueissues")
private String averagecostpastdueissues;
@JsonProperty("averagecostallissues")
private String averagecostallissues;
@JsonProperty("densityopenissues")
private String densityopenissues;
@JsonProperty("densitycloseissues")
private String densitycloseissues;
@JsonProperty("densitypastdueissues")
private String densitypastdueissues;
@JsonProperty("densityallissues")
private String densityallissues;
@JsonProperty("hours")
private String hours;
@JsonProperty("users")
private List<String> users = null;
@JsonProperty("usersopenissues")
private List<String> usersopenissues = null;
@JsonProperty("userscloseissues")
private List<String> userscloseissues = null;
@JsonProperty("userspastdueissues")
private List<String> userspastdueissues = null;
@JsonProperty("usersassigneeissues")
private List<String> usersassigneeissues = null;
@JsonProperty("usersaveragecostopenissues")
private List<String> usersaveragecostopenissues = null;
@JsonProperty("usersaveragecostcloseissues")
private List<String> usersaveragecostcloseissues = null;
@JsonProperty("usersaveragecostpastdueissues")
private List<String> usersaveragecostpastdueissues = null;
@JsonProperty("usersaveragecostassigneeissues")
private List<String> usersaveragecostassigneeissues = null;
@JsonProperty("usersdensityopenissues")
private List<String> usersdensityopenissues = null;
@JsonProperty("usersdensitycloseissues")
private List<String> usersdensitycloseissues = null;
@JsonProperty("usersdensitypastdueissues")
private List<String> usersdensitypastdueissues = null;
@JsonProperty("usersdensityassigneeissues")
private List<String> usersdensityassigneeissues = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("openissues")
public String getOpenissues() {
return openissues;
}

@JsonProperty("openissues")
public void setOpenissues(String openissues) {
this.openissues = openissues;
}

@JsonProperty("closeissues")
public String getCloseissues() {
return closeissues;
}

@JsonProperty("closeissues")
public void setCloseissues(String closeissues) {
this.closeissues = closeissues;
}

@JsonProperty("pastdueissues")
public String getPastdueissues() {
return pastdueissues;
}

@JsonProperty("pastdueissues")
public void setPastdueissues(String pastdueissues) {
this.pastdueissues = pastdueissues;
}

@JsonProperty("allissues")
public String getAllissues() {
return allissues;
}

@JsonProperty("allissues")
public void setAllissues(String allissues) {
this.allissues = allissues;
}

@JsonProperty("averagecostopenissues")
public String getAveragecostopenissues() {
return averagecostopenissues;
}

@JsonProperty("averagecostopenissues")
public void setAveragecostopenissues(String averagecostopenissues) {
this.averagecostopenissues = averagecostopenissues;
}

@JsonProperty("averagecostcloseissues")
public String getAveragecostcloseissues() {
return averagecostcloseissues;
}

@JsonProperty("averagecostcloseissues")
public void setAveragecostcloseissues(String averagecostcloseissues) {
this.averagecostcloseissues = averagecostcloseissues;
}

@JsonProperty("averagecostpastdueissues")
public String getAveragecostpastdueissues() {
return averagecostpastdueissues;
}

@JsonProperty("averagecostpastdueissues")
public void setAveragecostpastdueissues(String averagecostpastdueissues) {
this.averagecostpastdueissues = averagecostpastdueissues;
}

@JsonProperty("averagecostallissues")
public String getAveragecostallissues() {
return averagecostallissues;
}

@JsonProperty("averagecostallissues")
public void setAveragecostallissues(String averagecostallissues) {
this.averagecostallissues = averagecostallissues;
}

@JsonProperty("densityopenissues")
public String getDensityopenissues() {
return densityopenissues;
}

@JsonProperty("densityopenissues")
public void setDensityopenissues(String densityopenissues) {
this.densityopenissues = densityopenissues;
}

@JsonProperty("densitycloseissues")
public String getDensitycloseissues() {
return densitycloseissues;
}

@JsonProperty("densitycloseissues")
public void setDensitycloseissues(String densitycloseissues) {
this.densitycloseissues = densitycloseissues;
}

@JsonProperty("densitypastdueissues")
public String getDensitypastdueissues() {
return densitypastdueissues;
}

@JsonProperty("densitypastdueissues")
public void setDensitypastdueissues(String densitypastdueissues) {
this.densitypastdueissues = densitypastdueissues;
}

@JsonProperty("densityallissues")
public String getDensityallissues() {
return densityallissues;
}

@JsonProperty("densityallissues")
public void setDensityallissues(String densityallissues) {
this.densityallissues = densityallissues;
}

@JsonProperty("hours")
public String getHours() {
return hours;
}

@JsonProperty("hours")
public void setHours(String hours) {
this.hours = hours;
}

@JsonProperty("users")
public List<String> getUsers() {
return users;
}

@JsonProperty("users")
public void setUsers(List<String> users) {
this.users = users;
}

@JsonProperty("usersopenissues")
public List<String> getUsersopenissues() {
return usersopenissues;
}

@JsonProperty("usersopenissues")
public void setUsersopenissues(List<String> usersopenissues) {
this.usersopenissues = usersopenissues;
}

@JsonProperty("userscloseissues")
public List<String> getUserscloseissues() {
return userscloseissues;
}

@JsonProperty("userscloseissues")
public void setUserscloseissues(List<String> userscloseissues) {
this.userscloseissues = userscloseissues;
}

@JsonProperty("userspastdueissues")
public List<String> getUserspastdueissues() {
return userspastdueissues;
}

@JsonProperty("userspastdueissues")
public void setUserspastdueissues(List<String> userspastdueissues) {
this.userspastdueissues = userspastdueissues;
}

@JsonProperty("usersassigneeissues")
public List<String> getUsersassigneeissues() {
return usersassigneeissues;
}

@JsonProperty("usersassigneeissues")
public void setUsersassigneeissues(List<String> usersassigneeissues) {
this.usersassigneeissues = usersassigneeissues;
}

@JsonProperty("usersaveragecostopenissues")
public List<String> getUsersaveragecostopenissues() {
return usersaveragecostopenissues;
}

@JsonProperty("usersaveragecostopenissues")
public void setUsersaveragecostopenissues(List<String> usersaveragecostopenissues) {
this.usersaveragecostopenissues = usersaveragecostopenissues;
}

@JsonProperty("usersaveragecostcloseissues")
public List<String> getUsersaveragecostcloseissues() {
return usersaveragecostcloseissues;
}

@JsonProperty("usersaveragecostcloseissues")
public void setUsersaveragecostcloseissues(List<String> usersaveragecostcloseissues) {
this.usersaveragecostcloseissues = usersaveragecostcloseissues;
}

@JsonProperty("usersaveragecostpastdueissues")
public List<String> getUsersaveragecostpastdueissues() {
return usersaveragecostpastdueissues;
}

@JsonProperty("usersaveragecostpastdueissues")
public void setUsersaveragecostpastdueissues(List<String> usersaveragecostpastdueissues) {
this.usersaveragecostpastdueissues = usersaveragecostpastdueissues;
}

@JsonProperty("usersaveragecostassigneeissues")
public List<String> getUsersaveragecostassigneeissues() {
return usersaveragecostassigneeissues;
}

@JsonProperty("usersaveragecostassigneeissues")
public void setUsersaveragecostassigneeissues(List<String> usersaveragecostassigneeissues) {
this.usersaveragecostassigneeissues = usersaveragecostassigneeissues;
}

@JsonProperty("usersdensityopenissues")
public List<String> getUsersdensityopenissues() {
return usersdensityopenissues;
}

@JsonProperty("usersdensityopenissues")
public void setUsersdensityopenissues(List<String> usersdensityopenissues) {
this.usersdensityopenissues = usersdensityopenissues;
}

@JsonProperty("usersdensitycloseissues")
public List<String> getUsersdensitycloseissues() {
return usersdensitycloseissues;
}

@JsonProperty("usersdensitycloseissues")
public void setUsersdensitycloseissues(List<String> usersdensitycloseissues) {
this.usersdensitycloseissues = usersdensitycloseissues;
}

@JsonProperty("usersdensitypastdueissues")
public List<String> getUsersdensitypastdueissues() {
return usersdensitypastdueissues;
}

@JsonProperty("usersdensitypastdueissues")
public void setUsersdensitypastdueissues(List<String> usersdensitypastdueissues) {
this.usersdensitypastdueissues = usersdensitypastdueissues;
}

@JsonProperty("usersdensityassigneeissues")
public List<String> getUsersdensityassigneeissues() {
return usersdensityassigneeissues;
}

@JsonProperty("usersdensityassigneeissues")
public void setUsersdensityassigneeissues(List<String> usersdensityassigneeissues) {
this.usersdensityassigneeissues = usersdensityassigneeissues;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}