
package issue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "state",
    "description",
    "author",
    "milestone",
    "project_id",
    "assignees",
    "assignee",
    "updated_at",
    "closed_at",
    "closed_by",
    "id",
    "title",
    "created_at",
    "iid",
    "labels",
    "upvotes",
    "downvotes",
    "merge_requests_count",
    "user_notes_count",
    "due_date",
    "web_url",
    "time_stats",
    "confidential",
    "discussion_locked"
})
public class Issue {

    @JsonProperty("state")
    private String state;
    @JsonProperty("description")
    private String description;
    @JsonProperty("author")
    private Author author;
    @JsonProperty("milestone")
    private Milestone milestone;
    @JsonProperty("project_id")
    private String projectId;
    @JsonProperty("assignees")
    private List<Assignee> assignees = null;
    @JsonProperty("assignee")
    private Assignee_ assignee;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("closed_at")
    private String closedAt;
    @JsonProperty("closed_by")
    private ClosedBy closedBy;
    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("iid")
    private String iid;
    @JsonProperty("labels")
    private List<String> labels = null;
    @JsonProperty("upvotes")
    private String upvotes;
    @JsonProperty("downvotes")
    private String downvotes;
    @JsonProperty("merge_requests_count")
    private String mergeRequestsCount;
    @JsonProperty("user_notes_count")
    private String userNotesCount;
    @JsonProperty("due_date")
    private String dueDate;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("time_stats")
    private TimeStats timeStats;
    @JsonProperty("confidential")
    private String confidential;
    @JsonProperty("discussion_locked")
    private String discussionLocked;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("author")
    public Author getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(Author author) {
        this.author = author;
    }

    @JsonProperty("milestone")
    public Milestone getMilestone() {
        return milestone;
    }

    @JsonProperty("milestone")
    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    @JsonProperty("project_id")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("project_id")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("assignees")
    public List<Assignee> getAssignees() {
        return assignees;
    }

    @JsonProperty("assignees")
    public void setAssignees(List<Assignee> assignees) {
        this.assignees = assignees;
    }

    @JsonProperty("assignee")
    public Assignee_ getAssignee() {
        return assignee;
    }

    @JsonProperty("assignee")
    public void setAssignee(Assignee_ assignee) {
        this.assignee = assignee;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("closed_at")
    public String getClosedAt() {
        return closedAt;
    }

    @JsonProperty("closed_at")
    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    @JsonProperty("closed_by")
    public ClosedBy getClosedBy() {
        return closedBy;
    }

    @JsonProperty("closed_by")
    public void setClosedBy(ClosedBy closedBy) {
        this.closedBy = closedBy;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("iid")
    public String getIid() {
        return iid;
    }

    @JsonProperty("iid")
    public void setIid(String iid) {
        this.iid = iid;
    }

    @JsonProperty("labels")
    public List<String> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    @JsonProperty("upvotes")
    public String getUpvotes() {
        return upvotes;
    }

    @JsonProperty("upvotes")
    public void setUpvotes(String upvotes) {
        this.upvotes = upvotes;
    }

    @JsonProperty("downvotes")
    public String getDownvotes() {
        return downvotes;
    }

    @JsonProperty("downvotes")
    public void setDownvotes(String downvotes) {
        this.downvotes = downvotes;
    }

    @JsonProperty("merge_requests_count")
    public String getMergeRequestsCount() {
        return mergeRequestsCount;
    }

    @JsonProperty("merge_requests_count")
    public void setMergeRequestsCount(String mergeRequestsCount) {
        this.mergeRequestsCount = mergeRequestsCount;
    }

    @JsonProperty("user_notes_count")
    public String getUserNotesCount() {
        return userNotesCount;
    }

    @JsonProperty("user_notes_count")
    public void setUserNotesCount(String userNotesCount) {
        this.userNotesCount = userNotesCount;
    }

    @JsonProperty("due_date")
    public String getDueDate() {
        return dueDate;
    }

    @JsonProperty("due_date")
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    @JsonProperty("web_url")
    public String getWebUrl() {
        return webUrl;
    }

    @JsonProperty("web_url")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonProperty("time_stats")
    public TimeStats getTimeStats() {
        return timeStats;
    }

    @JsonProperty("time_stats")
    public void setTimeStats(TimeStats timeStats) {
        this.timeStats = timeStats;
    }

    @JsonProperty("confidential")
    public String getConfidential() {
        return confidential;
    }

    @JsonProperty("confidential")
    public void setConfidential(String confidential) {
        this.confidential = confidential;
    }

    @JsonProperty("discussion_locked")
    public String getDiscussionLocked() {
        return discussionLocked;
    }

    @JsonProperty("discussion_locked")
    public void setDiscussionLocked(String discussionLocked) {
        this.discussionLocked = discussionLocked;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @Override
    public String toString() {
        return "Issues [state:" + state + "\n" + 
        "description: " + description + "\n" + 
        "author: " + author + "\n" + 
        "milestone: " + milestone  + "\n"+ 
        "projectId: " + projectId + "\n" + 
        "assignees: " + assignees +"\n"+
        "assignee: " + assignee +"\n"+ 
        "updatedAt: " + updatedAt + "\n" + 
        "closedAt: " + closedAt + "\n" + 
        "closedBy: " + closedBy + "\n" + 
        "id: " + id + "\n" + 
        "title: " + title + "\n" + 
        "createdAt: " + createdAt + "\n" + 
        "iid: " + iid + "\n" + 
        "labels: " + labels + "\n" + 
        "upvotes: " + upvotes + "\n" + 
        "downvotes: " + downvotes+ "\n" + 
        "userNotesCount: " + userNotesCount + "\n" + 
        "dueDate: " + dueDate + "\n" + 
        "webUrl: " + webUrl + "\n" + 
        "confidential: " + confidential +  "\n" + 
        "discussionLocked: " + discussionLocked +  "\n" + 
        "timeStats: " + timeStats +  "]" + "\n";
    }/*
    @Override
        public String toString() {
        return "Issues $ [state: $ " + state + "$" + 
        "description: $ " + description + "$" + 
        "author: $ " + author + "$" + 
        "milestone: $ " + milestone  + "$"+ 
        "projectId: $ " + projectId + "$" + 
        "assignees: $ " + assignees +"$"+
        "assignee: $ " + assignee +"$"+ 
        "updatedAt: $ " + updatedAt + "$" + 
        "closedAt: $ " + closedAt + "$" + 
        "closedBy: $ " + closedBy + "$" + 
        "id: $ " + id + "$" + 
        "title: $ " + title + "$" + 
        "createdAt: $ " + createdAt + "$" + 
        "iid: $ " + iid + "$" + 
        "labels: $ " + labels + "$" + 
        "upvotes: $ " + upvotes + "$" + 
        "downvotes: $ " + downvotes+ "$" + 
        "userNotesCount: $ " + userNotesCount + "$" + 
        "dueDate: $ " + dueDate + "$" + 
        "webUrl: $ " + webUrl + "$" + 
        "confidential: $ " + confidential +  "$" + 
        "discussionLocked: $ " + discussionLocked +  "$" + 
        "timeStats: $ " + timeStats +  "]" + "\n";
    }*/

}
