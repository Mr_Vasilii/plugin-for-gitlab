
package project;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "commit_count",
    "storage_size",
    "repository_size",
    "lfs_objects_size",
    "job_artifacts_size"
})
public class Statistics {

    @JsonProperty("commit_count")
    private String commitCount;
    @JsonProperty("storage_size")
    private String storageSize;
    @JsonProperty("repository_size")
    private String repositorySize;
    @JsonProperty("lfs_objects_size")
    private String lfsObjectsSize;
    @JsonProperty("job_artifacts_size")
    private String jobArtifactsSize;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("commit_count")
    public String getCommitCount() {
        return commitCount;
    }

    @JsonProperty("commit_count")
    public void setCommitCount(String commitCount) {
        this.commitCount = commitCount;
    }

    @JsonProperty("storage_size")
    public String getStorageSize() {
        return storageSize;
    }

    @JsonProperty("storage_size")
    public void setStorageSize(String storageSize) {
        this.storageSize = storageSize;
    }

    @JsonProperty("repository_size")
    public String getRepositorySize() {
        return repositorySize;
    }

    @JsonProperty("repository_size")
    public void setRepositorySize(String repositorySize) {
        this.repositorySize = repositorySize;
    }

    @JsonProperty("lfs_objects_size")
    public String getLfsObjectsSize() {
        return lfsObjectsSize;
    }

    @JsonProperty("lfs_objects_size")
    public void setLfsObjectsSize(String lfsObjectsSize) {
        this.lfsObjectsSize = lfsObjectsSize;
    }

    @JsonProperty("job_artifacts_size")
    public String getJobArtifactsSize() {
        return jobArtifactsSize;
    }

    @JsonProperty("job_artifacts_size")
    public void setJobArtifactsSize(String jobArtifactsSize) {
        this.jobArtifactsSize = jobArtifactsSize;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
