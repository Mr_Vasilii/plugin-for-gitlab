
package project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "description",
    "default_branch",
    "visibility",
    "ssh_url_to_repo",
    "http_url_to_repo",
    "web_url",
    "readme_url",
    "tag_list",
    "owner",
    "name",
    "name_with_namespace",
    "path",
    "path_with_namespace",
    "issues_enabled",
    "open_issues_count",
    "merge_requests_enabled",
    "jobs_enabled",
    "wiki_enabled",
    "snippets_enabled",
    "resolve_outdated_diff_discussions",
    "container_registry_enabled",
    "created_at",
    "last_activity_at",
    "creator_id",
    "namespace",
    "import_status",
    "archived",
    "avatar_url",
    "shared_runners_enabled",
    "forks_count",
    "star_count",
    "runners_token",
    "public_jobs",
    "only_allow_merge_if_pipeline_succeeds",
    "only_allow_merge_if_all_discussions_are_resolved",
    "request_access_enabled",
    "merge_method",
    "statistics",
    "_links"
})
public class Project {

    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("default_branch")
    private String defaultBranch;
    @JsonProperty("visibility")
    private String visibility;
    @JsonProperty("ssh_url_to_repo")
    private String sshUrlToRepo;
    @JsonProperty("http_url_to_repo")
    private String httpUrlToRepo;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("readme_url")
    private String readmeUrl;
    @JsonProperty("tag_list")
    private List<String> tagList = null;
    @JsonProperty("owner")
    private Owner owner;
    @JsonProperty("name")
    private String name;
    @JsonProperty("name_with_namespace")
    private String nameWithNamespace;
    @JsonProperty("path")
    private String path;
    @JsonProperty("path_with_namespace")
    private String pathWithNamespace;
    @JsonProperty("issues_enabled")
    private String issuesEnabled;
    @JsonProperty("open_issues_count")
    private String openIssuesCount;
    @JsonProperty("merge_requests_enabled")
    private String mergeRequestsEnabled;
    @JsonProperty("jobs_enabled")
    private String jobsEnabled;
    @JsonProperty("wiki_enabled")
    private String wikiEnabled;
    @JsonProperty("snippets_enabled")
    private String snippetsEnabled;
    @JsonProperty("resolve_outdated_diff_discussions")
    private String resolveOutdatedDiffDiscussions;
    @JsonProperty("container_registry_enabled")
    private String containerRegistryEnabled;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("last_activity_at")
    private String lastActivityAt;
    @JsonProperty("creator_id")
    private String creatorId;
    @JsonProperty("namespace")
    private Namespace namespace;
    @JsonProperty("import_status")
    private String importStatus;
    @JsonProperty("archived")
    private String archived;
    @JsonProperty("avatar_url")
    private String avatarUrl;
    @JsonProperty("shared_runners_enabled")
    private String sharedRunnersEnabled;
    @JsonProperty("forks_count")
    private String forksCount;
    @JsonProperty("star_count")
    private String starCount;
    @JsonProperty("runners_token")
    private String runnersToken;
    @JsonProperty("public_jobs")
    private String publicJobs;
    @JsonProperty("only_allow_merge_if_pipeline_succeeds")
    private String onlyAllowMergeIfPipelineSucceeds;
    @JsonProperty("only_allow_merge_if_all_discussions_are_resolved")
    private String onlyAllowMergeIfAllDiscussionsAreResolved;
    @JsonProperty("request_access_enabled")
    private String requestAccessEnabled;
    @JsonProperty("merge_method")
    private String mergeMethod;
    @JsonProperty("statistics")
    private Statistics statistics;
    @JsonProperty("_links")
    private Links links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("default_branch")
    public String getDefaultBranch() {
        return defaultBranch;
    }

    @JsonProperty("default_branch")
    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    @JsonProperty("visibility")
    public String getVisibility() {
        return visibility;
    }

    @JsonProperty("visibility")
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    @JsonProperty("ssh_url_to_repo")
    public String getSshUrlToRepo() {
        return sshUrlToRepo;
    }

    @JsonProperty("ssh_url_to_repo")
    public void setSshUrlToRepo(String sshUrlToRepo) {
        this.sshUrlToRepo = sshUrlToRepo;
    }

    @JsonProperty("http_url_to_repo")
    public String getHttpUrlToRepo() {
        return httpUrlToRepo;
    }

    @JsonProperty("http_url_to_repo")
    public void setHttpUrlToRepo(String httpUrlToRepo) {
        this.httpUrlToRepo = httpUrlToRepo;
    }

    @JsonProperty("web_url")
    public String getWebUrl() {
        return webUrl;
    }

    @JsonProperty("web_url")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonProperty("readme_url")
    public String getReadmeUrl() {
        return readmeUrl;
    }

    @JsonProperty("readme_url")
    public void setReadmeUrl(String readmeUrl) {
        this.readmeUrl = readmeUrl;
    }

    @JsonProperty("tag_list")
    public List<String> getTagList() {
        return tagList;
    }

    @JsonProperty("tag_list")
    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    @JsonProperty("owner")
    public Owner getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name_with_namespace")
    public String getNameWithNamespace() {
        return nameWithNamespace;
    }

    @JsonProperty("name_with_namespace")
    public void setNameWithNamespace(String nameWithNamespace) {
        this.nameWithNamespace = nameWithNamespace;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonProperty("path_with_namespace")
    public String getPathWithNamespace() {
        return pathWithNamespace;
    }

    @JsonProperty("path_with_namespace")
    public void setPathWithNamespace(String pathWithNamespace) {
        this.pathWithNamespace = pathWithNamespace;
    }

    @JsonProperty("issues_enabled")
    public String getIssuesEnabled() {
        return issuesEnabled;
    }

    @JsonProperty("issues_enabled")
    public void setIssuesEnabled(String issuesEnabled) {
        this.issuesEnabled = issuesEnabled;
    }

    @JsonProperty("open_issues_count")
    public String getOpenIssuesCount() {
        return openIssuesCount;
    }

    @JsonProperty("open_issues_count")
    public void setOpenIssuesCount(String openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
    }

    @JsonProperty("merge_requests_enabled")
    public String getMergeRequestsEnabled() {
        return mergeRequestsEnabled;
    }

    @JsonProperty("merge_requests_enabled")
    public void setMergeRequestsEnabled(String mergeRequestsEnabled) {
        this.mergeRequestsEnabled = mergeRequestsEnabled;
    }

    @JsonProperty("jobs_enabled")
    public String getJobsEnabled() {
        return jobsEnabled;
    }

    @JsonProperty("jobs_enabled")
    public void setJobsEnabled(String jobsEnabled) {
        this.jobsEnabled = jobsEnabled;
    }

    @JsonProperty("wiki_enabled")
    public String getWikiEnabled() {
        return wikiEnabled;
    }

    @JsonProperty("wiki_enabled")
    public void setWikiEnabled(String wikiEnabled) {
        this.wikiEnabled = wikiEnabled;
    }

    @JsonProperty("snippets_enabled")
    public String getSnippetsEnabled() {
        return snippetsEnabled;
    }

    @JsonProperty("snippets_enabled")
    public void setSnippetsEnabled(String snippetsEnabled) {
        this.snippetsEnabled = snippetsEnabled;
    }

    @JsonProperty("resolve_outdated_diff_discussions")
    public String getResolveOutdatedDiffDiscussions() {
        return resolveOutdatedDiffDiscussions;
    }

    @JsonProperty("resolve_outdated_diff_discussions")
    public void setResolveOutdatedDiffDiscussions(String resolveOutdatedDiffDiscussions) {
        this.resolveOutdatedDiffDiscussions = resolveOutdatedDiffDiscussions;
    }

    @JsonProperty("container_registry_enabled")
    public String getContainerRegistryEnabled() {
        return containerRegistryEnabled;
    }

    @JsonProperty("container_registry_enabled")
    public void setContainerRegistryEnabled(String containerRegistryEnabled) {
        this.containerRegistryEnabled = containerRegistryEnabled;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("last_activity_at")
    public String getLastActivityAt() {
        return lastActivityAt;
    }

    @JsonProperty("last_activity_at")
    public void setLastActivityAt(String lastActivityAt) {
        this.lastActivityAt = lastActivityAt;
    }

    @JsonProperty("creator_id")
    public String getCreatorId() {
        return creatorId;
    }

    @JsonProperty("creator_id")
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    @JsonProperty("namespace")
    public Namespace getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(Namespace namespace) {
        this.namespace = namespace;
    }

    @JsonProperty("import_status")
    public String getImportStatus() {
        return importStatus;
    }

    @JsonProperty("import_status")
    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    @JsonProperty("archived")
    public String getArchived() {
        return archived;
    }

    @JsonProperty("archived")
    public void setArchived(String archived) {
        this.archived = archived;
    }

    @JsonProperty("avatar_url")
    public String getAvatarUrl() {
        return avatarUrl;
    }

    @JsonProperty("avatar_url")
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @JsonProperty("shared_runners_enabled")
    public String getSharedRunnersEnabled() {
        return sharedRunnersEnabled;
    }

    @JsonProperty("shared_runners_enabled")
    public void setSharedRunnersEnabled(String sharedRunnersEnabled) {
        this.sharedRunnersEnabled = sharedRunnersEnabled;
    }

    @JsonProperty("forks_count")
    public String getForksCount() {
        return forksCount;
    }

    @JsonProperty("forks_count")
    public void setForksCount(String forksCount) {
        this.forksCount = forksCount;
    }

    @JsonProperty("star_count")
    public String getStarCount() {
        return starCount;
    }

    @JsonProperty("star_count")
    public void setStarCount(String starCount) {
        this.starCount = starCount;
    }

    @JsonProperty("runners_token")
    public String getRunnersToken() {
        return runnersToken;
    }

    @JsonProperty("runners_token")
    public void setRunnersToken(String runnersToken) {
        this.runnersToken = runnersToken;
    }

    @JsonProperty("public_jobs")
    public String getPublicJobs() {
        return publicJobs;
    }

    @JsonProperty("public_jobs")
    public void setPublicJobs(String publicJobs) {
        this.publicJobs = publicJobs;
    }

    @JsonProperty("only_allow_merge_if_pipeline_succeeds")
    public String getOnlyAllowMergeIfPipelineSucceeds() {
        return onlyAllowMergeIfPipelineSucceeds;
    }

    @JsonProperty("only_allow_merge_if_pipeline_succeeds")
    public void setOnlyAllowMergeIfPipelineSucceeds(String onlyAllowMergeIfPipelineSucceeds) {
        this.onlyAllowMergeIfPipelineSucceeds = onlyAllowMergeIfPipelineSucceeds;
    }

    @JsonProperty("only_allow_merge_if_all_discussions_are_resolved")
    public String getOnlyAllowMergeIfAllDiscussionsAreResolved() {
        return onlyAllowMergeIfAllDiscussionsAreResolved;
    }

    @JsonProperty("only_allow_merge_if_all_discussions_are_resolved")
    public void setOnlyAllowMergeIfAllDiscussionsAreResolved(String onlyAllowMergeIfAllDiscussionsAreResolved) {
        this.onlyAllowMergeIfAllDiscussionsAreResolved = onlyAllowMergeIfAllDiscussionsAreResolved;
    }

    @JsonProperty("request_access_enabled")
    public String getRequestAccessEnabled() {
        return requestAccessEnabled;
    }

    @JsonProperty("request_access_enabled")
    public void setRequestAccessEnabled(String requestAccessEnabled) {
        this.requestAccessEnabled = requestAccessEnabled;
    }

    @JsonProperty("merge_method")
    public String getMergeMethod() {
        return mergeMethod;
    }

    @JsonProperty("merge_method")
    public void setMergeMethod(String mergeMethod) {
        this.mergeMethod = mergeMethod;
    }

    @JsonProperty("statistics")
    public Statistics getStatistics() {
        return statistics;
    }

    @JsonProperty("statistics")
    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @Override
    public String toString() {
        return "Project[ID: " + id +"\n" +  "description: " + description +"\n" + 
"default_branch: " + defaultBranch +"\n" + 
"ssh_url_to_repo: " + sshUrlToRepo +"\n" + 
"http_url_to_repo: " + httpUrlToRepo +"\n" + 
"web_url: " + webUrl + "\n" + 
"readme_url: " + readmeUrl + "\n" + 
"name: " + name + "\n" + 
"name_with_namespace: " + nameWithNamespace + "\n" + 
"path: " + path + "\n" + 
"path_with_namespace: " + pathWithNamespace + "\n" + 
"created_at: " + createdAt + "\n" + 
"last_activity_at: " + lastActivityAt + "\n" + 
"forks_count: " + forksCount + "\n" + 
"avatar_url: "+ avatarUrl+ "\n" + 
"star_count: "+ starCount +"\n" + 
"tagList: "+ tagList +"\n" + links +
"]"+"\n";
    }
}