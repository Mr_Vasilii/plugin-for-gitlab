
package user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "username",
    "email",
    "name",
    "state",
    "avatar_url",
    "web_url",
    "created_at",
    "is_admin",
    "bio",
    "location",
    "skype",
    "linkedin",
    "twitter",
    "website_url",
    "organization",
    "last_sign_in_at",
    "confirmed_at",
    "theme_id",
    "last_activity_on",
    "color_scheme_id",
    "projects_limit",
    "current_sign_in_at",
    "identities",
    "can_create_group",
    "can_create_project",
    "two_factor_enabled",
    "external",
    "private_profile"
})
public class User {

    @JsonProperty("id")
    private String id;
    @JsonProperty("username")
    private String username;
    @JsonProperty("email")
    private String email;
    @JsonProperty("name")
    private String name;
    @JsonProperty("state")
    private String state;
    @JsonProperty("avatar_url")
    private String avatarUrl;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("is_admin")
    private String isAdmin;
    @JsonProperty("bio")
    private String bio;
    @JsonProperty("location")
    private String location;
    @JsonProperty("skype")
    private String skype;
    @JsonProperty("linkedin")
    private String linkedin;
    @JsonProperty("twitter")
    private String twitter;
    @JsonProperty("website_url")
    private String websiteUrl;
    @JsonProperty("organization")
    private String organization;
    @JsonProperty("last_sign_in_at")
    private String lastSignInAt;
    @JsonProperty("confirmed_at")
    private String confirmedAt;
    @JsonProperty("theme_id")
    private String themeId;
    @JsonProperty("last_activity_on")
    private String lastActivityOn;
    @JsonProperty("color_scheme_id")
    private String colorSchemeId;
    @JsonProperty("projects_limit")
    private String projectsLimit;
    @JsonProperty("current_sign_in_at")
    private String currentSignInAt;
    @JsonProperty("identities")
    private List<Identity> identities = null;
    @JsonProperty("can_create_group")
    private String canCreateGroup;
    @JsonProperty("can_create_project")
    private String canCreateProject;
    @JsonProperty("two_factor_enabled")
    private String twoFactorEnabled;
    @JsonProperty("external")
    private String external;
    @JsonProperty("private_profile")
    private String privateProfile;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public User() {
    }

    /**
     * 
     * @param webUrl
     * @param themeId
     * @param location
     * @param external
     * @param colorSchemeId
     * @param state
     * @param lastSignInAt
     * @param projectsLimit
     * @param id
     * @param lastActivityOn
     * @param currentSignInAt
     * @param twitter
     * @param username
     * @param organization
     * @param canCreateProject
     * @param createdAt
     * @param avatarUrl
     * @param name
     * @param websiteUrl
     * @param linkedin
     * @param identities
     * @param canCreateGroup
     * @param bio
     * @param privateProfile
     * @param email
     * @param isAdmin
     * @param twoFactorEnabled
     * @param confirmedAt
     * @param skype
     */
    public User(String id, String username, String email, String name, String state, String avatarUrl, String webUrl, String createdAt, String isAdmin, String bio, String location, String skype, String linkedin, String twitter, String websiteUrl, String organization, String lastSignInAt, String confirmedAt, String themeId, String lastActivityOn, String colorSchemeId, String projectsLimit, String currentSignInAt, List<Identity> identities, String canCreateGroup, String canCreateProject, String twoFactorEnabled, String external, String privateProfile) {
        super();
        this.id = id;
        this.username = username;
        this.email = email;
        this.name = name;
        this.state = state;
        this.avatarUrl = avatarUrl;
        this.webUrl = webUrl;
        this.createdAt = createdAt;
        this.isAdmin = isAdmin;
        this.bio = bio;
        this.location = location;
        this.skype = skype;
        this.linkedin = linkedin;
        this.twitter = twitter;
        this.websiteUrl = websiteUrl;
        this.organization = organization;
        this.lastSignInAt = lastSignInAt;
        this.confirmedAt = confirmedAt;
        this.themeId = themeId;
        this.lastActivityOn = lastActivityOn;
        this.colorSchemeId = colorSchemeId;
        this.projectsLimit = projectsLimit;
        this.currentSignInAt = currentSignInAt;
        this.identities = identities;
        this.canCreateGroup = canCreateGroup;
        this.canCreateProject = canCreateProject;
        this.twoFactorEnabled = twoFactorEnabled;
        this.external = external;
        this.privateProfile = privateProfile;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("avatar_url")
    public String getAvatarUrl() {
        return avatarUrl;
    }

    @JsonProperty("avatar_url")
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @JsonProperty("web_url")
    public String getWebUrl() {
        return webUrl;
    }

    @JsonProperty("web_url")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("is_admin")
    public String getIsAdmin() {
        return isAdmin;
    }

    @JsonProperty("is_admin")
    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    @JsonProperty("bio")
    public String getBio() {
        return bio;
    }

    @JsonProperty("bio")
    public void setBio(String bio) {
        this.bio = bio;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("skype")
    public String getSkype() {
        return skype;
    }

    @JsonProperty("skype")
    public void setSkype(String skype) {
        this.skype = skype;
    }

    @JsonProperty("linkedin")
    public String getLinkedin() {
        return linkedin;
    }

    @JsonProperty("linkedin")
    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    @JsonProperty("twitter")
    public String getTwitter() {
        return twitter;
    }

    @JsonProperty("twitter")
    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    @JsonProperty("website_url")
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    @JsonProperty("website_url")
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    @JsonProperty("organization")
    public String getOrganization() {
        return organization;
    }

    @JsonProperty("organization")
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @JsonProperty("last_sign_in_at")
    public String getLastSignInAt() {
        return lastSignInAt;
    }

    @JsonProperty("last_sign_in_at")
    public void setLastSignInAt(String lastSignInAt) {
        this.lastSignInAt = lastSignInAt;
    }

    @JsonProperty("confirmed_at")
    public String getConfirmedAt() {
        return confirmedAt;
    }

    @JsonProperty("confirmed_at")
    public void setConfirmedAt(String confirmedAt) {
        this.confirmedAt = confirmedAt;
    }

    @JsonProperty("theme_id")
    public String getThemeId() {
        return themeId;
    }

    @JsonProperty("theme_id")
    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }

    @JsonProperty("last_activity_on")
    public String getLastActivityOn() {
        return lastActivityOn;
    }

    @JsonProperty("last_activity_on")
    public void setLastActivityOn(String lastActivityOn) {
        this.lastActivityOn = lastActivityOn;
    }

    @JsonProperty("color_scheme_id")
    public String getColorSchemeId() {
        return colorSchemeId;
    }

    @JsonProperty("color_scheme_id")
    public void setColorSchemeId(String colorSchemeId) {
        this.colorSchemeId = colorSchemeId;
    }

    @JsonProperty("projects_limit")
    public String getProjectsLimit() {
        return projectsLimit;
    }

    @JsonProperty("projects_limit")
    public void setProjectsLimit(String projectsLimit) {
        this.projectsLimit = projectsLimit;
    }

    @JsonProperty("current_sign_in_at")
    public String getCurrentSignInAt() {
        return currentSignInAt;
    }

    @JsonProperty("current_sign_in_at")
    public void setCurrentSignInAt(String currentSignInAt) {
        this.currentSignInAt = currentSignInAt;
    }

    @JsonProperty("identities")
    public List<Identity> getIdentities() {
        return identities;
    }

    @JsonProperty("identities")
    public void setIdentities(List<Identity> identities) {
        this.identities = identities;
    }

    @JsonProperty("can_create_group")
    public String getCanCreateGroup() {
        return canCreateGroup;
    }

    @JsonProperty("can_create_group")
    public void setCanCreateGroup(String canCreateGroup) {
        this.canCreateGroup = canCreateGroup;
    }

    @JsonProperty("can_create_project")
    public String getCanCreateProject() {
        return canCreateProject;
    }

    @JsonProperty("can_create_project")
    public void setCanCreateProject(String canCreateProject) {
        this.canCreateProject = canCreateProject;
    }

    @JsonProperty("two_factor_enabled")
    public String getTwoFactorEnabled() {
        return twoFactorEnabled;
    }

    @JsonProperty("two_factor_enabled")
    public void setTwoFactorEnabled(String twoFactorEnabled) {
        this.twoFactorEnabled = twoFactorEnabled;
    }

    @JsonProperty("external")
    public String getExternal() {
        return external;
    }

    @JsonProperty("external")
    public void setExternal(String external) {
        this.external = external;
    }

    @JsonProperty("private_profile")
    public String getPrivateProfile() {
        return privateProfile;
    }

    @JsonProperty("private_profile")
    public void setPrivateProfile(String privateProfile) {
        this.privateProfile = privateProfile;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @Override
    public String toString() {
        return "User[ID:" + id + ", username: " + username + ", name: " + name + ", state: " + state + ", avatarUrl: " + avatarUrl + ", webUrl: " + webUrl + 	", createdAt: " + createdAt +", isAdmin: " + isAdmin +", bio: " + bio +", location: " + location +", skype: " + skype +", linkedin: " + linkedin +", twitter: " + twitter +", websiteUrl: " + websiteUrl +", organization: " + organization +", lastSignInAt: " + lastSignInAt +", confirmedAt: " + confirmedAt +", themeId: " + themeId +", lastActivityOn: " + lastActivityOn +", colorSchemeId: " + colorSchemeId +", projectsLimit: " + projectsLimit +", currentSignInAt: " + currentSignInAt +", identities: " + identities +", canCreateGroup: " + canCreateGroup +", canCreateProject: " + canCreateProject +", twoFactorEnabled: " + twoFactorEnabled +", external: " + external +", privateProfile: " + privateProfile +"]" + "\n";
    }
}
