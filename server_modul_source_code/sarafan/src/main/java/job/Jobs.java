
package jobs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "commit",
    "coverage",
    "created_at",
    "started_at",
    "finished_at",
    "duration",
    "artifacts_expire_at",
    "id",
    "name",
    "pipeline",
    "ref",
    "artifacts",
    "runner",
    "stage",
    "status",
    "tag",
    "web_url",
    "user"
})
public class Jobs {

    @JsonProperty("commit")
    private Commit commit;
    @JsonProperty("coverage")
    private String coverage;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("started_at")
    private String startedAt;
    @JsonProperty("finished_at")
    private String finishedAt;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("artifacts_expire_at")
    private String artifactsExpireAt;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pipeline")
    private Pipeline pipeline;
    @JsonProperty("ref")
    private String ref;
    @JsonProperty("artifacts")
    private List<Object> artifacts = null;
    @JsonProperty("runner")
    private String runner;
    @JsonProperty("stage")
    private String stage;
    @JsonProperty("status")
    private String status;
    @JsonProperty("tag")
    private String tag;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("user")
    private User user;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Jobs() {
    }

    /**
     * 
     * @param webUrl
     * @param status
     * @param tag
     * @param startedAt
     * @param stage
     * @param artifacts
     * @param id
     * @param ref
     * @param duration
     * @param commit
     * @param coverage
     * @param createdAt
     * @param pipeline
     * @param finishedAt
     * @param name
     * @param runner
     * @param artifactsExpireAt
     * @param user
     */
    public Jobs(Commit commit, String coverage, String createdAt, String startedAt, String finishedAt, String duration, String artifactsExpireAt, String id, String name, Pipeline pipeline, String ref, List<Object> artifacts, String runner, String stage, String status, String tag, String webUrl, User user) {
        super();
        this.commit = commit;
        this.coverage = coverage;
        this.createdAt = createdAt;
        this.startedAt = startedAt;
        this.finishedAt = finishedAt;
        this.duration = duration;
        this.artifactsExpireAt = artifactsExpireAt;
        this.id = id;
        this.name = name;
        this.pipeline = pipeline;
        this.ref = ref;
        this.artifacts = artifacts;
        this.runner = runner;
        this.stage = stage;
        this.status = status;
        this.tag = tag;
        this.webUrl = webUrl;
        this.user = user;
    }

    @JsonProperty("commit")
    public Commit getCommit() {
        return commit;
    }

    @JsonProperty("commit")
    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    @JsonProperty("coverage")
    public String getCoverage() {
        return coverage;
    }

    @JsonProperty("coverage")
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("started_at")
    public String getStartedAt() {
        return startedAt;
    }

    @JsonProperty("started_at")
    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    @JsonProperty("finished_at")
    public String getFinishedAt() {
        return finishedAt;
    }

    @JsonProperty("finished_at")
    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("artifacts_expire_at")
    public String getArtifactsExpireAt() {
        return artifactsExpireAt;
    }

    @JsonProperty("artifacts_expire_at")
    public void setArtifactsExpireAt(String artifactsExpireAt) {
        this.artifactsExpireAt = artifactsExpireAt;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pipeline")
    public Pipeline getPipeline() {
        return pipeline;
    }

    @JsonProperty("pipeline")
    public void setPipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    @JsonProperty("ref")
    public String getRef() {
        return ref;
    }

    @JsonProperty("ref")
    public void setRef(String ref) {
        this.ref = ref;
    }

    @JsonProperty("artifacts")
    public List<Object> getArtifacts() {
        return artifacts;
    }

    @JsonProperty("artifacts")
    public void setArtifacts(List<Object> artifacts) {
        this.artifacts = artifacts;
    }

    @JsonProperty("runner")
    public String getRunner() {
        return runner;
    }

    @JsonProperty("runner")
    public void setRunner(String runner) {
        this.runner = runner;
    }

    @JsonProperty("stage")
    public String getStage() {
        return stage;
    }

    @JsonProperty("stage")
    public void setStage(String stage) {
        this.stage = stage;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    @JsonProperty("web_url")
    public String getWebUrl() {
        return webUrl;
    }

    @JsonProperty("web_url")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
       		    @Override
    public String toString() {
        return "Jobs [ID:" + id + ", "+ "\n" + "user: " + user + ", name: " + name + ", status: " + status + ", tag: " + tag + ", webUrl: " + webUrl + ", stage: " + stage + ", runner: " + runner + ", artifacts: " + artifacts + ", ref: " + ref + ", artifactsExpireAt: " + artifactsExpireAt + ", duration: " + duration + ", finishedAt: " + finishedAt + ", startedAt: " + startedAt +  ", finishedAt: " + finishedAt +  ", coverage: " + coverage + ", " + "\n"  + "pipeline: " + pipeline  +  "commit: " + commit + "]" + "\n";
    }
}
