package restfulforgitlab.sarafan;

import com.fasterxml.jackson.databind.ObjectMapper;
import commits.Commits;
import issue.Issue;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import project.Project;
import user.User;
import ClassForApi.*;
import issue.Assignee;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStreamWriter;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import project.UsersToProject;


@SpringBootApplication
public class SarafanApplication {

    private static int check = 0;
    //private static String issuesUrl[];
    private static ArrayList<String> issuesUrl = new ArrayList<String>();
    private static ArrayList<String> projectUrl = new ArrayList<String>();
    private static ArrayList<Project> Projects = new ArrayList<Project>();
    private static ArrayList<User> Users = new ArrayList<User>();
    private static ArrayList<Issue> Issues = new ArrayList<Issue>();
    private static ArrayList<String> projectID = new ArrayList<String>();
    private static ArrayList<Commits> Commits = new ArrayList<Commits>();
    private static ArrayList<UsersToProject> UsersToProjects = new ArrayList<UsersToProject>();
    
    private static ArrayList<UsersApi> UsersApiArr = new ArrayList<UsersApi>();
    static ArrayList<IssuesApi> IssuesApiArr = new ArrayList<IssuesApi>();
    private static ArrayList<ProjectsApi> ProjectsApiArr = new ArrayList<ProjectsApi>();
    
    private static DecimalFormat df = new DecimalFormat("###.###");
    static UsersApi[] UsersJsonApi ;
    static ProjectsApi[] ProjectsJsonApi;
    static IssuesApi[] IssuesJsonApi;
    static String gitlab_url;
    static String gitlab_token;
	public static void main(String[] args) throws FileNotFoundException, IOException {
            System.err.println("HELLO WORLD");
            checkConfingFile();
           // checkConnectForGitLab();
//            if(checkConfingFile()){
//                if(checkConnectForGitLab()){
//                    gitlab();
//                }
//            }
        SpringApplication app = new SpringApplication(SarafanApplication.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "9375"));
        
        app.run(args);

	}

    static void gitlab() {
        issuesUrl.clear();
        projectUrl.clear();
        Projects.clear();
        Users.clear();
        Issues.clear();
        projectID.clear();
        Commits.clear();
        UsersApiArr.clear();
        IssuesApiArr.clear();
        ProjectsApiArr.clear();
        String url = gitlab_url;
        String apiUrl = url + "api/v4/";
        String token = "?private_token="+gitlab_token;//EcgVXUBzchAsQHkD291a
        String users = "users";
        String projects = "projects";
        String commit = "/projects/:id/repository/commits";
        
        int mmm =0;
        //обработка апи
        httpUser(apiUrl+users+token,1); 
        httpUser(apiUrl+projects+token,2); 
        for(String iU : issuesUrl){
            int index = 1;
            check = 0;
            iU=iU.substring(iU.indexOf("api"),iU.length());
            while(check == 0) {
               httpUser((url+iU+token+"&page="+String.valueOf(index)),3);
               System.out.println("Check page - " + mmm++);
               index++;
            }
        }
            System.out.println("Projects");
            for (Project pr : Projects) {
//                System.out.println(pr); // name ID
                projectID.add(pr.getId());
            }


             // Creating object of Organisation 
CreateIssuesJson();
CreateUsersJson();
CreateProjectJson();
    }

    

    
    private static void CreateIssuesJson(){
            IssuesApi org = new IssuesApi(); 

            System.out.println("Issues");
            for (Issue is : Issues) {
                org = new IssuesApi();
//                System.out.println(is);// state1  title1  projectId1  assignee1 assignees createdAt dueDate closedAt
                org.setTitle("ID: " + is.getId() + "; Name: " + is.getTitle());
                org.setProject(is.getProjectId());
                org.setState(is.getState());
                if(is.getAssignee()!=null){
                    org.setUserto(is.getAssignee().getUsername());
                    String assignees = "";
                    for(Assignee in : is.getAssignees()){
                        assignees += in.getUsername()+ ", ";
                    }
                    org.setUsersto(assignees);
                }else{
                    org.setUserto("null");
                    org.setUsersto("null");
                }
                //на сколько часов просрочена ошибка TODO
                Date date = new Date();
                Date parsingDate = new Date();
                if(is.getDueDate()!=null){
                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd"); 
                    String str = is.getDueDate(); 
                    str = str.replace('T',' ');
                    try {
                    parsingDate = ft.parse(str);
                    }catch (ParseException e) { 
                        System.out.println("Нераспаршена с помощью " + ft); 
                    }
                    if(parsingDate.before(date) && "opened".equals(is.getState())){
                        long t1 = parsingDate.getTime();
                        long t2 = date.getTime();

                        org.setDuedate(String.valueOf((t2-t1)/3600000.000));
                        System.err.println(String.valueOf((t2-t1)/3600000.000));
                    }else{org.setDuedate("null");}
                }else{
                    org.setDuedate("null");
                }
                
                //количество часов потраченных на ошибку
                date = new Date();
                parsingDate = new Date();
                long start = 0;
                long end = 0;
                SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 
                String str = is.getCreatedAt(); 
                str = str.replace('T',' ');
                try {
                    parsingDate = ft.parse(str);
                    start = parsingDate.getTime();
                }catch (ParseException e) { 
                    System.out.println("Нераспаршена с помощью " + ft); 
                }
                if(is.getClosedAt()!=null){
                    str = is.getClosedAt(); 
                    str = str.replace('T',' ');
                    try {
                        parsingDate = ft.parse(str);
                        end = parsingDate.getTime();
                    }catch (ParseException e) { 
                        System.out.println("Нераспаршена с помощью " + ft); 
                    }
                }else{
                    end = date.getTime();
                    
                }
                org.setHours(String.valueOf((end-start)/3600000.000));
                
                
            IssuesApiArr.add(org);
            }
  
//        // Creating Object of ObjectMapper define in Jakson Api 
//        ObjectMapper Obj = new ObjectMapper(); 
//  
//        try { 
//            // get Oraganisation object as a json string 
//            String jsonStr = Obj.writeValueAsString(IssuesApiArr); 
//            // Displaying JSON String 
//            IssuesJsonApi = new ObjectMapper().readValue(jsonStr, IssuesApi[].class);
//        } 
//  
//        catch (IOException e) { 
//            e.printStackTrace(); 
//        } 
//        System.err.println("DONE");
    }
    
    
        private static void CreateUsersJson(){
        UsersApi org = new UsersApi(); 
        System.out.println("Users");
        for (User us : Users) {
            org = new UsersApi(); 
//            System.out.println(us); // username
            String username = us.getUsername();
            org.setName(username);
            int openissues = 0;
            int closeissues = 0;
            int assigneeissues = 0;
            int pastduedateissue = 0;
            float timeallissues = 0;
            float timeopenissues = 0;
            float timecloseissues = 0;
            float timepastduedateissue = 0;
            for (IssuesApi is :IssuesApiArr){
                if(username.equals(is.getUserto())){
                    if("closed".equals(is.getState())){
                        timecloseissues +=Float.valueOf(is.getHours());
                        closeissues++;
                    }
                    if("opened".equals(is.getState())){
                        timeopenissues +=Float.valueOf(is.getHours());
                        openissues++;
                    }
                    if(!"null".equals(is.getDuedate())){
                        timepastduedateissue +=Float.valueOf(is.getHours());
                        pastduedateissue++;
                    }
                    if(is.getUsersto().indexOf(username)!=-1){
                        timeallissues +=Float.valueOf(is.getHours());
                        assigneeissues++;
                    }
                }
            }
            org.setAssigneeissues(String.valueOf(assigneeissues));
            org.setOpenissues(String.valueOf(openissues));
            org.setPastdueissues(String.valueOf(pastduedateissue));
            org.setCloseissues(String.valueOf(closeissues));
            
            org.setAveragecostassigneeissues(String.valueOf(df.format(timeallissues/assigneeissues)));
            org.setAveragecostcloseissues(String.valueOf(df.format(timecloseissues/closeissues)));
            org.setAveragecostopenissues(String.valueOf(df.format(timeopenissues/openissues)));
            org.setAveragecostpastdueissues(String.valueOf(df.format(timepastduedateissue/pastduedateissue)));
            

            org.setDensityassigneeissues(String.valueOf(df.format((float)assigneeissues/assigneeissues)));
            org.setDensitycloseissues(String.valueOf(df.format((float)closeissues/assigneeissues)));
            org.setDensityopenissues(String.valueOf(df.format((float)openissues/assigneeissues)));
            org.setDensitypastdueissues(String.valueOf(df.format((float)pastduedateissue/assigneeissues)));
            UsersApiArr.add(org);
        }
        // Insert the data into the object 
  
        // Creating Object of ObjectMapper define in Jakson Api 
        ObjectMapper Obj = new ObjectMapper(); 
  
        try { 
            // get Oraganisation object as a json string 
            String jsonStr = Obj.writeValueAsString(UsersApiArr); 
            // Displaying JSON String 
            UsersJsonApi = new ObjectMapper().readValue(jsonStr, UsersApi[].class);
        } 
  
        catch (IOException e) { 
            e.printStackTrace(); 
        } 
            System.err.println("DONE");
    }
    
    
    
    
    private static void CreateProjectJson(){
        String token = "?private_token="+gitlab_token;
         List<String> UsersToProject = new ArrayList<String>();
        
         
         List<String> usersopenissues = new ArrayList<String>();
         List<String> userscloseissues = new ArrayList<String>();
         List<String> userspastdueissues = new ArrayList<String>();
         List<String> usersassigneeissues = new ArrayList<String>();
         
         List<String> usersaveragecostopenissues = new ArrayList<String>();
         List<String> usersaveragecostcloseissues = new ArrayList<String>();
         List<String> usersaveragecostpastdueissues = new ArrayList<String>();
         List<String> usersaveragecostassigneeissues = new ArrayList<String>();
         
         List<String> usersdensityopenissues = new ArrayList<String>();
         List<String> usersdensitycloseissues = new ArrayList<String>();
         List<String> usersdensitypastdueissues = new ArrayList<String>();
         List<String> usersdensityassigneeissues = new ArrayList<String>();
        //List<String> supplierNames = Arrays.asList("sup1", "sup2", "sup3");
        ProjectsApi org = new ProjectsApi(); 
        int allissuesinproject = 0;
        int openissuesinproject = 0;
        int closeissuesinproject = 0;
        int pastduedateissueinproject = 0;
        System.out.println("Projects");
        Date date = new Date();
        long end = date.getTime();
        Date parsingDate = new Date();
        for (Project pr : Projects) {                
            long start = 0;
            
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss"); 
            String str = pr.getCreatedAt(); 
            str = str.replace('T',' ');
            try {
                parsingDate = ft.parse(str);
                start = parsingDate.getTime();
            }catch (ParseException e) { 
                System.out.println("Нераспаршена с помощью " + ft); 
            }
            String linkusers = pr.getLinks().getMembers();
            linkusers = linkusers.substring(linkusers.indexOf("api/v4/"), linkusers.length());
            linkusers = gitlab_url + linkusers + token + "&count=9999";
            httpUser(linkusers,5);

            for(UsersToProject utp :UsersToProjects){
                UsersToProject.add(utp.getUsername());
            }
            
            org = new ProjectsApi(); 
            String id = pr.getId();
            org.setId(id);
            org.setName(pr.getName());
            org.setUsers(UsersToProject);
            org.setHours(String.valueOf((end-start)/3600000.000));
            int allissues = 0;
            int openissues = 0;
            int closeissues = 0;
            int pastduedateissue = 0;
            float timeallissues = 0;
            float timeopenissues = 0;
            float timecloseissues = 0;
            float timepastduedateissue = 0;
            for (IssuesApi is :IssuesApiArr){
                if(id.equals(is.getProject())){
                    allissues++;
//                    System.err.println(is.getHours());
                    timeallissues += Float.valueOf(is.getHours());
                    if("closed".equals(is.getState())){
                        timecloseissues +=Float.valueOf(is.getHours());
                        closeissues++;
                    }
                    if("opened".equals(is.getState())){
                        timeopenissues +=Float.valueOf(is.getHours());
                        openissues++;
                    }
                    if(!"null".equals(is.getDuedate())){
                        timepastduedateissue +=Float.valueOf(is.getHours());
                        pastduedateissue++;
                    }
                }
            }
            allissuesinproject = allissues;
            openissuesinproject = openissues;
            closeissuesinproject = closeissues;
            pastduedateissueinproject = pastduedateissue;
            org.setAllissues(String.valueOf(allissues));
            org.setCloseissues(String.valueOf(closeissues));
            org.setOpenissues(String.valueOf(openissues));
            org.setPastdueissues(String.valueOf(pastduedateissue));
            
            org.setAveragecostallissues(String.valueOf(df.format(timeallissues/allissues)));
            org.setAveragecostcloseissues(String.valueOf(df.format(timecloseissues/closeissues)));
            org.setAveragecostopenissues(String.valueOf(df.format(timeopenissues/openissues)));
            org.setAveragecostpastdueissues(String.valueOf(df.format(timepastduedateissue/pastduedateissue)));
            

            org.setDensityallissues(String.valueOf(df.format((float)allissues/allissues)));
            org.setDensitycloseissues(String.valueOf(df.format((float)closeissues/allissues)));
            org.setDensityopenissues(String.valueOf(df.format((float)openissues/allissues)));
            org.setDensitypastdueissues(String.valueOf(df.format((float)pastduedateissue/allissues)));
            
            
            for(String user : UsersToProject){
                 allissues = 0;
                 openissues = 0;
                 closeissues = 0;
                 pastduedateissue = 0;
                 timeallissues = 0;
                 timeopenissues = 0;
                 timecloseissues = 0;
                 timepastduedateissue = 0;
                for (IssuesApi is :IssuesApiArr){
                    if(user.equals(is.getUserto())){
                        if(id.equals(is.getProject())){
                            allissues++;
        //                    System.err.println(is.getHours());
                            timeallissues += Float.valueOf(is.getHours());
                            if("closed".equals(is.getState())){
                                timecloseissues +=Float.valueOf(is.getHours());
                                closeissues++;
                            }
                            if("opened".equals(is.getState())){
                                timeopenissues +=Float.valueOf(is.getHours());
                                openissues++;
                            }
                            if(!"null".equals(is.getDuedate())){
                                timepastduedateissue +=Float.valueOf(is.getHours());
                                pastduedateissue++;
                            }
                        }
                    }
                }
                
                usersassigneeissues.add(String.valueOf(allissues));
                userscloseissues.add(String.valueOf(closeissues));
                usersopenissues.add(String.valueOf(openissues));
                userspastdueissues.add(String.valueOf(pastduedateissue));

                usersaveragecostassigneeissues.add(String.valueOf(df.format(timeallissues/allissuesinproject)));
                usersaveragecostcloseissues.add(String.valueOf(df.format(timecloseissues/closeissuesinproject)));
                usersaveragecostopenissues.add(String.valueOf(df.format(timeopenissues/openissuesinproject)));
                usersaveragecostpastdueissues.add(String.valueOf(df.format(timepastduedateissue/pastduedateissueinproject)));

                usersdensityassigneeissues.add(String.valueOf(df.format((float)allissues/allissuesinproject)));
                usersdensitycloseissues.add(String.valueOf(df.format((float)closeissues/allissuesinproject)));
                usersdensityopenissues.add(String.valueOf(df.format((float)openissues/allissuesinproject)));
                usersdensitypastdueissues.add(String.valueOf(df.format((float)pastduedateissue/allissuesinproject)));

       
            }
            
            org.setUsersassigneeissues(usersassigneeissues);
            org.setUsersaveragecostassigneeissues(usersaveragecostassigneeissues);
            org.setUsersaveragecostcloseissues(usersaveragecostcloseissues);
            org.setUsersaveragecostopenissues(usersaveragecostopenissues);
            org.setUsersaveragecostpastdueissues(usersaveragecostpastdueissues);
            org.setUserscloseissues(userscloseissues);
            org.setUsersdensityassigneeissues(usersdensityassigneeissues);
            org.setUsersdensitycloseissues(usersdensitycloseissues);
            org.setUsersdensityopenissues(usersdensityopenissues);
            org.setUsersdensitypastdueissues(usersdensitypastdueissues);
            org.setUsersopenissues(usersopenissues);
            org.setUserspastdueissues(userspastdueissues);
            
            ProjectsApiArr.add(org);
        }

  
        // Creating Object of ObjectMapper define in Jakson Api 
        ObjectMapper Obj = new ObjectMapper(); 
  
        try { 
            // get Oraganisation object as a json string 
            String jsonStr = Obj.writeValueAsString(ProjectsApiArr); 
            // Displaying JSON String 
            ProjectsJsonApi = new ObjectMapper().readValue(jsonStr, ProjectsApi[].class);
        } 

        catch (IOException e) { 
            e.printStackTrace(); 
        } 
      
    }
    
    private static Object delElAr(Object[] a) {
        int n = a.length;

        for ( int i = 0, m = 0; i != n; i++, n = m )
        {
            for ( int j = m = i + 1; j != n; j++ )
            {
                if ( a[j].equals(a[i])  == false)
                {
                    if ( m != j ) a[m] = a[j];
                    m++;
                }
            }
        }
        if ( n != a.length )
        {
            Object[] b = new Object[n];
            for ( int i = 0; i < n; i++ ) b[i] = a[i];

            a = b;
        }

        return a;
    }

    static boolean checkConfingFile() throws FileNotFoundException, IOException {
        String fileName = GreetingController.fileName;
        boolean checkCF = false;
            if ((new File(fileName)).exists()) {

                    Properties props = new Properties();
                    props.load(new FileInputStream(new File(fileName)));

                    gitlab_url = props.getProperty("gitlab_url");
                    gitlab_token = props.getProperty("gitlab_token");
                    checkCF = true;
            }
        return checkCF;
    }

    static boolean checkConnectForGitLab() {
                String query = gitlab_url + "api/v4/issues?private_token="+gitlab_token;
String mycheck = null;
        //api/v4/issues
        HttpURLConnection connection = null;
     PrintWriter writer = null;
    try {
         connection = (HttpURLConnection) new URL(query).openConnection();
         connection.setRequestMethod("GET");
         connection.setUseCaches(false);
         connection.setConnectTimeout(1500);
         connection.setReadTimeout(1500);
         connection.connect();
         StringBuilder sb = new StringBuilder();
         if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
             BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
             String line;
             while ((line = in.readLine()) != null){
                sb.append(line);
                sb.append("\n");
             }

         
mycheck = sb.toString();
         } else {
             mycheck = "fail: " + connection.getResponseCode() + " , " + connection.getResponseMessage();
         }
    } catch (Throwable cause){
                 cause.printStackTrace();}
                 finally {
                 if (connection != null){
                 connection.disconnect();
                 }
         }
    if(mycheck.indexOf("fail")==-1){
        return true;
    }else{
        return false;
    } 
    }

    int dnaCode;
       public boolean equals(SarafanApplication man) {
       return this.dnaCode ==  man.dnaCode;
   }

    private static void httpUser(String query, int option) {
        
        HttpURLConnection connection = null;
     PrintWriter writer = null;
    try {
         connection = (HttpURLConnection) new URL(query).openConnection();
         connection.setRequestMethod("GET");
         connection.setUseCaches(false);
         connection.setConnectTimeout(1500);
         connection.setReadTimeout(1500);
         connection.connect();

         StringBuilder sb = new StringBuilder();
         
         if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
             BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
             
             String line;
             while ((line = in.readLine()) != null){
                sb.append(line);
                sb.append("\n");
             }

            ObjectMapper objectMapper = new ObjectMapper();
            switch (option){
                case 1:
                    User[] user = objectMapper.readValue(sb.toString(), User[].class);
                    for (User u : user) {
                        Users.add(u);
                    }
                break;
                
                case 2:
                    Project[] project = objectMapper.readValue(sb.toString(), Project[].class);
                    int j = 0;
                    for (Project p : project) {
                        Projects.add(p);
                        issuesUrl.add(p.getLinks().getIssues());
                    }
                break;
                
                case 3:
                  //  System.out.println(sb.toString());
                    if(sb.toString().length()<5){
                        check=1;
                        break;
                    }else{
                        Issue[] issue= (objectMapper.readValue(sb.toString(), Issue[].class));
                        for (Issue i : issue) {
                            Issues.add(i);
                        }
                        break;
                    }
                case 4:
                    if(sb.toString().length()<5){
                        check=1;
                        break;
                    }else{
                        Commits[] commit = objectMapper.readValue(sb.toString(), Commits[].class);
                 
                    for (Commits c : commit) {
                        Commits.add(c);
                    }
                    break;
                    }
                case 5:
                    UsersToProject[] UsersToProject = objectMapper.readValue(sb.toString(), UsersToProject[].class);
                    for (UsersToProject utp : UsersToProject) {
                        UsersToProjects.add(utp);
                    }
                break;
            }
         } else {
             System.out.println("fail: " + connection.getResponseCode() + " , " + connection.getResponseMessage());
         }
    } catch (Throwable cause){
                 cause.printStackTrace();}
                 finally {
                 if (connection != null){
                 connection.disconnect();
                 }
         }
    }
}
