package restfulforgitlab.sarafan;

import ClassForApi.IssuesApi;
import ClassForApi.ProjectsApi;
import ClassForApi.UsersApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import static restfulforgitlab.sarafan.SarafanApplication.*;

@RestController
public class GreetingController {
        private List<Map<String, String>> messagesI = new ArrayList<Map<String, String>>();

        static ArrayList<IssuesApi> IssuesApiArrControllerAll = new ArrayList<IssuesApi>();
        static ArrayList<IssuesApi> IssuesApiArrControllerAMod = new ArrayList<IssuesApi>();
        private String prtoken;
        private IssuesApi[] error;
        static String  fileName = "confing.ini";
        
        @RequestMapping(value = "/users" , method = RequestMethod.GET, produces = "application/json; charset=utf-8")
        @CrossOrigin(origins = "*", allowedHeaders = "*")
	public UsersApi[] users() {
            return SarafanApplication.UsersJsonApi;
	}
        
        @RequestMapping(value = "/issues" , method = RequestMethod.GET, produces = "application/json; charset=utf-8")
        @CrossOrigin(origins = "*", allowedHeaders = "*")
	public IssuesApi[] issues(@RequestParam (required = false) String state,@RequestParam (required = false) String assignee,@RequestParam (required = false) String pastduedate, @RequestParam (required = false) String assignees,@RequestParam (required = false) String project) {
            IssuesApiArrControllerAll.clear();
            IssuesApiArrControllerAll.addAll(SarafanApplication.IssuesApiArr);
            for(IssuesApi x:SarafanApplication.IssuesApiArr){
                System.err.println(x);
            }
            System.err.println(state);
            System.err.println(assignee);
            System.err.println(assignees);
            if (assignee!=null && assignees!=null){
                System.err.println("1");
                return error;
            }else{
                System.err.println("2");
                if(state!=null){
                    for(IssuesApi x : IssuesApiArrControllerAll){
                        if(state.equals(x.getState())){
                            IssuesApiArrControllerAMod.add(x);
                        }
                    }
                    IssuesApiArrControllerAll.clear();
                    IssuesApiArrControllerAll.addAll(IssuesApiArrControllerAMod);
                    IssuesApiArrControllerAMod.clear();
                }
                if(project!=null){
                    for(IssuesApi x : IssuesApiArrControllerAll){
                        System.err.println(x);
                        if(project.equals(x.getProject())){
                            IssuesApiArrControllerAMod.add(x);
                        }
                    }
                    IssuesApiArrControllerAll.clear();
                    IssuesApiArrControllerAll.addAll(IssuesApiArrControllerAMod);
                    IssuesApiArrControllerAMod.clear();
                }
                if(assignee!=null){
                    for(IssuesApi x : IssuesApiArrControllerAll){
                        if(assignee.equals(x.getUserto())){
                            IssuesApiArrControllerAMod.add(x);
                        }
                    }
                    IssuesApiArrControllerAll.clear();
                    IssuesApiArrControllerAll.addAll(IssuesApiArrControllerAMod);
                    IssuesApiArrControllerAMod.clear();
                }
                if(assignees!=null){
                    for(IssuesApi x : IssuesApiArrControllerAll){
                        if(x.getUsersto().indexOf(assignees)!=-1){
                            IssuesApiArrControllerAMod.add(x);
                        }
                    }
                    IssuesApiArrControllerAll.clear();
                    IssuesApiArrControllerAll.addAll(IssuesApiArrControllerAMod);
                    IssuesApiArrControllerAMod.clear();
                }
                if(pastduedate!=null){
                    for(IssuesApi x : IssuesApiArrControllerAll){
                        if(!"null".equals(x.getDuedate()) && x.getDuedate()!=null){
                            IssuesApiArrControllerAMod.add(x);
                        }
                    }
                    IssuesApiArrControllerAll.clear();
                    IssuesApiArrControllerAll.addAll(IssuesApiArrControllerAMod);
                    IssuesApiArrControllerAMod.clear();
                }
                
                ObjectMapper Obj = new ObjectMapper(); 

                try { 
                    // get Oraganisation object as a json string 
                    String jsonStr = Obj.writeValueAsString(IssuesApiArrControllerAll); 
                    // Displaying JSON String 
                    IssuesJsonApi = new ObjectMapper().readValue(jsonStr, IssuesApi[].class);
                } 

                catch (IOException e) { 
                    e.printStackTrace(); 
                } 
                return IssuesJsonApi;
            }
            
	}
        
        @RequestMapping(value = "/projects" , method = RequestMethod.GET, produces = "application/json; charset=utf-8")
        @CrossOrigin(origins = "*", allowedHeaders = "*")
	public ProjectsApi[] projects() {
            return SarafanApplication.ProjectsJsonApi;
	}
        

        @CrossOrigin(origins = "*", allowedHeaders = "*")
        @GetMapping("/up")
	public void updateData() {
            SarafanApplication.gitlab();
	}
        
        @RequestMapping(value = "/create_confing" , method = RequestMethod.GET, produces = "application/json; charset=utf-8")
        @CrossOrigin(origins = "*", allowedHeaders = "*")
	public String getToken(@RequestParam (required = true) String token, @RequestParam (required = true) String url) throws IOException {
            this.prtoken=token;
            SarafanApplication.gitlab_url = url;
            SarafanApplication.gitlab_token = token;
            System.err.println(url + " " + token);
            File f = new File(fileName);
            //Создайте новый файл
            // Убедитесь, что он не существует 
            if (f.createNewFile())
                System.out.println("File created");
            else
                System.out.println("File already exists");
            
            try(FileWriter writer = new FileWriter(fileName, false))
            {
                // запись всей строки
                String text = "gitlab_url = " + url;
                writer.write(text);
                // запись по символам
                writer.append('\n');
                text = "gitlab_token = " + token;
                writer.write(text);
                writer.flush();
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
            return "Token and url is save";
            
	}
        
        @RequestMapping(value = "/test_connect" , method = RequestMethod.GET, produces = "application/json; charset=utf-8")
        @CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Map<String, String>>  getTtest() throws IOException {
            String cf = String.valueOf(SarafanApplication.checkConfingFile());
            String conn = String.valueOf(SarafanApplication.checkConnectForGitLab());
            messagesI.clear();
            messagesI = new ArrayList<Map<String, String>>(){{{
                add(new HashMap<String, String>() {{{put("confing",cf);put("connect",conn);put("тест","111");}}
                });
            }}};
            return messagesI;
	}
}